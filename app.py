from flask import Flask, render_template
from flask_login import LoginManager

import views

lm = LoginManager()

@lm.user_loader
def load_user(token):
    return views.get_user(token)

def create_app():
    app = Flask(__name__)
    app.config.from_object("settings")
    app.add_url_rule("/", view_func=views.login_page, methods= ["GET", "POST"])
    app.add_url_rule("/login", view_func=views.login_page, methods= ["GET", "POST"])
    app.add_url_rule("/register", view_func=views.register_page, methods = ["GET","POST"])
    app.add_url_rule("/logout", view_func=views.logout_page, methods = ["GET", "POST"])
    app.add_url_rule("/calendar_user", view_func=views.calendar_user)
    app.add_url_rule("/bookings_user", view_func=views.bookings_user)
    app.add_url_rule("/example_booking", view_func=views.example_booking)
    app.add_url_rule("/default", view_func=views.default_user, methods= ["GET", "POST"])

    lm.init_app(app)
    lm.login_view = "login_page"

    return app

if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port = 5000, debug = True)