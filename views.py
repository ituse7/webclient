from flask import render_template, request, url_for, flash, redirect, jsonify, make_response
from flask_login import UserMixin, login_user, login_required, logout_user, current_user
import slugify
import requests
import json as js

api = "http://localhost:8000"

class User(UserMixin):
    def __init__(self, token):
        self.token = token
        self.active = True
    
    def get_id(self):
        return self.token

    @property
    def is_active(self):
        return self.active

def get_user(token):
    if token == get_cookie():
        return User(token)
    else:
        return None

def login_page():
    if current_user.is_authenticated:
        return redirect(url_for("calendar_user"))
    if request.method ==   "GET":
        return render_template("login.html")
    else:
        mail = request.form["mail"]
        password = request.form["password"]
        json = f"""{{"username": "{mail}", "password": "{password}" }}"""
        r = requests.post(api+"/users/access-token", data=json)
        if r.status_code != 200:
            flash("An error occured.")
            return redirect( url_for("login_page"))
        r = js.loads(r.text)
        user = User(r["access_token"])
        login_user(user)
        user = requests.get(api+"/users/me", headers= {"Authorization": "Bearer {}".format(r["access_token"])})
        user = js.loads(user.text)
        resp = make_response(render_template("calendar_user.html", r = user))
        resp.set_cookie("access_token", r["access_token"])
        return resp

def register_page():
    if request.method == "GET":
        return render_template("register.html")
    else:
        mail = request.form["mail"]
        name = request.form["name"]
        surname = request.form["surname"]
        password = request.form["password"]
        confirm = request.form["confirm"]
        if password != confirm:
            flash("Your confirmation must be the same as your password.")
            return redirect(url_for("register_page"))
        fullname = name + " " + surname
        json = f"""{{"email": "{mail}", "password": "{password}", "name": "{fullname}"}}"""
        r = requests.post(api+"/users", data=json)
        if r.status_code != 200:
            flash("An error occured.")
        else:
            flash("Registration successful!")
            return redirect(url_for("login_page"))
        return render_template("register.html")

@login_required
def logout_page():
    flash("You have logged out.")
    resp = make_response(redirect(url_for("login_page")))
    logout_user()
    resp.set_cookie("access_token", "", expires=0)
    return resp

@login_required
def calendar_user():
    token = get_cookie()
    r = requests.get(api+"/users/me", headers= {"Authorization": "Bearer {}".format(token)})
    r = js.loads(r.text)
    calendar = requests.get(api+"/calendars/default", headers= {"Authorization": "Bearer {}".format(token)})
    if calendar.status_code != 200:
        return redirect(url_for("default_user"))
    calendar = js.loads(calendar.text)
    start = int(calendar["open_time"][:2])
    end = int(calendar["close_time"][:2])
    working = calendar["work_days"]
    return render_template("calendar_user.html", r=r, start=start, end=end, working=working)
@login_required
def default_user():
    if request.method == "GET":
        token = get_cookie()
        r = requests.get(api+"/users/me", headers= {"Authorization": "Bearer {}".format(token)})
        r = js.loads(r.text)
        return render_template("default_user.html", r=r)
    else:
        token = get_cookie()
        working = ",".join(day for day in request.form if request.form[day] == "on")
        name = request.form["name"]
        slug = slugify.slugify(name)
        open_time = request.form["start"]
        if len(open_time) == 4:
            open_time = "0"+open_time
        close_time = request.form["end"]
        if len(close_time) == 4:
            close_time = "0"+open_time
        json = f"""{{"name": "{name}",
                "slug": "{slug}",
                "open_time": "{open_time}",
                "close_time": "{close_time}",
                "work_days": "{working}",
                "timezone": "Europe/Istanbul",
                "default_duration": 30,
                "rest_duretion": 15,
                "is_default": true}}"""
        r = requests.post(api+"/calendars", data=json, headers= {"Authorization": "Bearer {}".format(token)})
        return redirect(url_for("calendar_user"))
@login_required
def bookings_user():
    return render_template("bookings_user.html")
@login_required
def example_booking():
    return render_template("view_attendees.html")

def get_cookie():
    access_token = request.cookies.get("access_token")
    return access_token