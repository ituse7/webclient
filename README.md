# MeetMe API

MeetMe frontend for managing calendar bookings

---

## Install

```bash
# Clone the project
$ git clone https://gitlab.com/ituse7/webclient.git meetmeweb
# Go to the project directory
$ cd meetmeweb
# Install project requirements.
$ pip3 install flask
$ pip3 install flask_login
$ pip3 install requests
$ pip3 install python_slugify
# Run the app
$ python3 app.py
```

## Configuration

Change the url for api in views.py.

---
TODO: improve documentation when project is complete.

&copy; 2020 Emin Mastizada, Aisenur Yoldas, Anil Ozlu, Halit Yagar, Nada Malek. MIT Licenced. 
